import 'package:flutter_netflix/api/api.dart';
import 'package:flutter_netflix/model/configuration.dart';
import 'package:flutter_netflix/model/season.dart';
import 'package:flutter_netflix/model/tmdb_image.dart';
import 'package:flutter_netflix/model/trending_videos.dart';
import 'package:flutter_netflix/model/user.dart';
import '../model/movie.dart';

class TMDBRepository {
  final TMDB _client = TMDB();

  Future<List<Movie>> getTrending({type = 'all', time = 'week'}) async {
    return (await _client.getTrending(type: type, time: time))
        .map((item) => Movie.fromJson(item))
        .toList();
  }

  Future<Configuration> getConfiguration() async {
    return Configuration.fromJson(await _client.getConfiguration());
  }

  Future<Movie> getDetails(id, type) async {
    return Movie.fromJson(
      await _client.getDetails(id, type),
      // medialType: type, details: true,
    );
  }

  Future<Season> getSeason(id, season) async {
    return Season.fromJson(await _client.getSeason(id, season));
  }

  Future<List<Movie>> discover(type) async {
    return (await _client.discover(type))
        .map((item) => Movie.fromJson(
              item,
              //  medialType: type,
            ))
        .toList();
  }

  Future<TMDBImages> getImages(id, type) async {
    return TMDBImages.fromJson(await _client.getImages(id, type));
  }

  // Future<TmdbVideos> getVideos(id, type) async {
  //   return TmdbVideos.fromJson(await _client.getVideos(id, type));
  // }

  Future<Map<String, dynamic>> registerUser(
      Map<String, String> registerUserData) async {
    return await _client.registerUser(registerUserData);
  }

  Future<Map<String, dynamic>> loginUser(
      Map<String, String> loginUserData) async {
    return await _client.loginUser(loginUserData);
  }

  Future<List<TrendingVideos>> getTrendingVideosdata({type, time}) async {
    return (await _client.getTrendingVideoData(type: type, time: time))
        .map((e) => TrendingVideos.fromJson(e))
        .toList();
  }

  Future<Map<String, dynamic>> createProfile(
      Map<String, dynamic> createProfileData) async {
    return await _client.createProfile(createProfileData);
  }

  Future<User> loggedUserData(int id) async {
    return User.fromJson(await _client.getUsers(id: id));
  }

  Future<Map<String, dynamic>> editUserProfile(
      int id, Map<String, dynamic> editUserData) async {
    return await _client.editUser(editUserData, id: id);
  }

  Future<Map<String, dynamic>> deleteUserProfile(int id) async {
    return await _client.deleteUser(id: id);
  }
}
