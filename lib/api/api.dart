import 'dart:convert';
import 'package:flutter_netflix/constants/variable.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TMDB {
  final endpoint = 'https://api.themoviedb.org/3';

  // Get a free key at: https://www.themoviedb.org/documentation/api
  final apiKey = '410ebf3c72ac8e8c6aff6b90ce8ad12a';

  Future<List<Map<String, dynamic>>> getTrending(
      {type = 'all', time = 'week'}) async {
    var result = await http
        // .get(Uri.parse(home));
        .get(Uri.parse('$endpoint/trending/$type/$time?api_key=$apiKey'));
    var body = jsonDecode(result.body);
    return List.castFrom<dynamic, Map<String, dynamic>>(body['results']);
  }

  Future<Map<String, dynamic>> getConfiguration() async {
    var result =
        await http.get(Uri.parse('$endpoint/configuration?api_key=$apiKey'));
    return jsonDecode(result.body);
  }

  Future<Map<String, dynamic>> getDetails(id, type) async {
    var result =
        await http.get(Uri.parse('$endpoint/$type/$id?api_key=$apiKey'));
    return jsonDecode(result.body);
  }

  Future<Map<String, dynamic>> getSeason(id, season) async {
    var result = await http
        .get(Uri.parse('$endpoint/tv/$id/season/$season?api_key=$apiKey'));
    return jsonDecode(result.body);
  }

  Future<List<Map<String, dynamic>>> discover(type) async {
    var result =
        await http.get(Uri.parse('$endpoint/discover/$type?api_key=$apiKey'));
    var body = jsonDecode(result.body);
    return List.castFrom<dynamic, Map<String, dynamic>>(body['results']);
  }

  Future<Map<String, dynamic>> getImages(id, type) async {
    var result =
        await http.get(Uri.parse('$endpoint/$type/$id/images?api_key=$apiKey'));
    return jsonDecode(result.body);
  }

  Future<Map<String, dynamic>> getVideos(id, type) async {
    var result = await http
        .get(Uri.parse('$endpoint/$type/$id/watch/providers?api_key=$apiKey'));
    var data = jsonDecode(result.body);
    return data;
  }

  Future<Map<String, dynamic>> registerUser(
      Map<String, String> registerUserData) async {
    EasyLoading.show();
    try {
      final response = await http.post(
        Uri.parse(register),
        headers: <String, String>{
          'Content-Type': 'application/json',
        },
        body: jsonEncode(registerUserData),
      );
      var result = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.dismiss();
        Fluttertoast.showToast(msg: 'Register successfully');
        return result;
      } else {
        Fluttertoast.showToast(msg: result['error']['message']);
        EasyLoading.dismiss();
      }
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
      EasyLoading.dismiss();
    }
    return {};
  }

  Future<Map<String, dynamic>> loginUser(
      Map<String, String> loginUserData) async {
    EasyLoading.show();

    try {
      final response = await http.post(
        Uri.parse(login),
        headers: <String, String>{
          'Content-Type': 'application/json',
        },
        body: jsonEncode(loginUserData),
      );
      var result = json.decode(response.body);
      if (response.statusCode == 200) {
        final prefs = await SharedPreferences.getInstance();
        prefs.setString('jwt_token', result['jwt']);
        prefs.setInt('user_id', result['user']['id']);
        Fluttertoast.showToast(msg: 'Login successfully');
        return result;
      } else {
        Fluttertoast.showToast(msg: result['error']['message']);
        EasyLoading.dismiss();
      }
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
      EasyLoading.dismiss();
    }
    return {};
  }

  Future<List<dynamic>> getTrendingVideoData(
      {type = 'movie', time = 'week'}) async {
    EasyLoading.show();
    try {
      final prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('jwt_token');
      var response = await http.get(
          Uri.parse(
              '$home?filters[show_type][\$eqi]=$type&filters[trending][\$eqi]=$time'),
          headers: {
            'Authorization': 'Bearer $token',
            'Content-Type': 'application/json'
          });
      var result = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.dismiss();
        return result['data'];
      } else {
        Fluttertoast.showToast(msg: result['error']['message']);
        EasyLoading.dismiss();
      }
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
      EasyLoading.dismiss();
    }
    return [];
  }

  Future<Map<String, dynamic>> createProfile(
      Map<String, dynamic> createProfileUserData) async {
    EasyLoading.show();
    try {
      final prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('jwt_token');
      final response = await http.post(
        Uri.parse(profile),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: jsonEncode(createProfileUserData),
      );
      var result = json.decode(response.body);
      if (response.statusCode == 200) {
        Fluttertoast.showToast(msg: 'Profile Successfully Created');
        return result;
      } else {
        Fluttertoast.showToast(msg: result['error']['message']);
        EasyLoading.dismiss();
      }
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
      EasyLoading.dismiss();
    }
    return {};
  }

  Future<Map<String, dynamic>> getUsers({int id = 1}) async {
    EasyLoading.show();
    try {
      final prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('jwt_token');
      var response = await http.get(Uri.parse('$user/$id?populate=*'),
          headers: {
            'Authorization': 'Bearer $token',
            'Content-Type': 'application/json'
          });
      var result = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.dismiss();
        return result;
      } else {
        Fluttertoast.showToast(msg: result['error']['message']);
        EasyLoading.dismiss();
      }
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
      EasyLoading.dismiss();
    }
    return {};
  }

  Future<Map<String, dynamic>> editUser(Map<String, dynamic> editedUserData,
      {int id = 1}) async {
    EasyLoading.show();
    try {
      final prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('jwt_token');
      var response = await http.put(Uri.parse('$profile/$id'),
          body: jsonEncode(editedUserData),
          headers: {
            'Authorization': 'Bearer $token',
            'Content-Type': 'application/json'
          });
      var result = json.decode(response.body);
      if (response.statusCode == 200) {
        Fluttertoast.showToast(msg: 'Profile Successfully Edited');
        EasyLoading.dismiss();
        return result;
      } else {
        Fluttertoast.showToast(msg: result['error']['message']);
        EasyLoading.dismiss();
      }
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
      EasyLoading.dismiss();
    }
    return {};
  }

  Future<Map<String, dynamic>> deleteUser({int id = 1}) async {
    EasyLoading.show();
    try {
      final prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('jwt_token');
      var response = await http.delete(Uri.parse('$profile/$id'), headers: {
        'Authorization': 'Bearer $token',
        'Content-Type': 'application/json'
      });
      var result = json.decode(response.body);
      if (response.statusCode == 200) {
        Fluttertoast.showToast(msg: 'Profile Deleted');
        EasyLoading.dismiss();
        return result;
      } else {
        Fluttertoast.showToast(msg: result['error']['message']);
        EasyLoading.dismiss();
      }
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
      EasyLoading.dismiss();
    }
    return {};
  }
}
