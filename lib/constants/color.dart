import 'package:flutter/material.dart';

Color hexToInt(String hexString) {
  // ignore: unnecessary_null_comparison
  if (hexString == '' || hexString == 'null' || hexString == null) {
    hexString = '#FFFFFF';
  }
  final buffer = StringBuffer();
  if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
  buffer.write(hexString.replaceFirst('#', ''));
  return Color(int.parse(buffer.toString(), radix: 16));
}

class AppColors {
  static Color white = hexToInt("#FFFFFF");
  static Color black = hexToInt("#000000");
  static Color red = hexToInt("#FF0000");
  static Color transparent = hexToInt('#000000').withOpacity(0);
}
