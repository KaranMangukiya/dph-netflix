import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

//host url & endpoint of api
const hostUrl = "http://192.168.29.19:1337/api";
const register = '$hostUrl/auth/local/register';
const login = '$hostUrl/auth/local';
const home = '$hostUrl/videos';
const profile = '$hostUrl/profiles';
const user = '$hostUrl/users';

//controller of textformfield
TextEditingController userNameController = TextEditingController();
TextEditingController emailController = TextEditingController();
TextEditingController passwordController = TextEditingController();
TextEditingController profileNameController = TextEditingController();
TextEditingController editProfileNameController = TextEditingController();

///for token
String? token;
Future<String?> getUserToken() async {
  var refs = await SharedPreferences.getInstance();
  var token = refs.getString("jwt_token");
  return token;
}
