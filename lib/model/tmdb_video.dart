// // To parse this JSON data, do
// //
// //     final tmdbVideos = tmdbVideosFromJson(jsonString);

// import 'dart:convert';

// class TmdbVideos {
//   TmdbVideos({
//     this.id,
//     this.results,
//   });

//   int? id;
//   Results? results;

//   factory TmdbVideos.fromRawJson(String str) =>
//       TmdbVideos.fromJson(json.decode(str));

//   String toRawJson() => json.encode(toJson());

//   factory TmdbVideos.fromJson(Map<String, dynamic> json) => TmdbVideos(
//         id: json["id"],
//         results:
//             json["results"] == null ? null : Results.fromJson(json["results"]),
//       );

//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "results": results?.toJson(),
//       };
// }

// class Results {
//   Results({
//     this.au,
//     this.ca,
//     this.de,
//     this.es,
//     this.fr,
//     this.gb,
//     this.resultsIn,
//     this.it,
//     this.kr,
//     this.mx,
//     this.us,
//   });

//   Au? au;
//   Ca? ca;
//   Au? de;
//   Au? es;
//   Au? fr;
//   Au? gb;
//   In? resultsIn;
//   Au? it;
//   Au? kr;
//   Mx? mx;
//   Ca? us;

//   factory Results.fromRawJson(String str) => Results.fromJson(json.decode(str));

//   String toRawJson() => json.encode(toJson());

//   factory Results.fromJson(Map<String, dynamic> json) => Results(
//         au: json["AU"] == null ? null : Au.fromJson(json["AU"]),
//         ca: json["CA"] == null ? null : Ca.fromJson(json["CA"]),
//         de: json["DE"] == null ? null : Au.fromJson(json["DE"]),
//         es: json["ES"] == null ? null : Au.fromJson(json["ES"]),
//         fr: json["FR"] == null ? null : Au.fromJson(json["FR"]),
//         gb: json["GB"] == null ? null : Au.fromJson(json["GB"]),
//         resultsIn: json["IN"] == null ? null : In.fromJson(json["IN"]),
//         it: json["IT"] == null ? null : Au.fromJson(json["IT"]),
//         kr: json["KR"] == null ? null : Au.fromJson(json["KR"]),
//         mx: json["MX"] == null ? null : Mx.fromJson(json["MX"]),
//         us: json["US"] == null ? null : Ca.fromJson(json["US"]),
//       );

//   Map<String, dynamic> toJson() => {
//         "AU": au?.toJson(),
//         "CA": ca?.toJson(),
//         "DE": de?.toJson(),
//         "ES": es?.toJson(),
//         "FR": fr?.toJson(),
//         "GB": gb?.toJson(),
//         "IN": resultsIn?.toJson(),
//         "IT": it?.toJson(),
//         "KR": kr?.toJson(),
//         "MX": mx?.toJson(),
//         "US": us?.toJson(),
//       };
// }

// class Au {
//   Au({
//     this.link,
//     this.free,
//   });

//   String? link;
//   List<Free>? free;

//   factory Au.fromRawJson(String str) => Au.fromJson(json.decode(str));

//   String toRawJson() => json.encode(toJson());

//   factory Au.fromJson(Map<String, dynamic> json) => Au(
//         link: json["link"],
//         free: json["free"] == null
//             ? []
//             : List<Free>.from(json["free"]!.map((x) => Free.fromJson(x))),
//       );

//   Map<String, dynamic> toJson() => {
//         "link": link,
//         "free": free == null
//             ? []
//             : List<dynamic>.from(free!.map((x) => x.toJson())),
//       };
// }

// class Free {
//   Free({
//     this.logoPath,
//     this.providerId,
//     this.providerName,
//     this.displayPriority,
//   });

//   String? logoPath;
//   int? providerId;
//   String? providerName;
//   int? displayPriority;

//   factory Free.fromRawJson(String str) => Free.fromJson(json.decode(str));

//   String toRawJson() => json.encode(toJson());

//   factory Free.fromJson(Map<String, dynamic> json) => Free(
//         logoPath: json["logo_path"],
//         providerId: json["provider_id"],
//         providerName: json["provider_name"],
//         displayPriority: json["display_priority"],
//       );

//   Map<String, dynamic> toJson() => {
//         "logo_path": logoPath,
//         "provider_id": providerId,
//         "provider_name": providerName,
//         "display_priority": displayPriority,
//       };
// }

// class Ca {
//   Ca({
//     this.link,
//     this.free,
//     this.flatrate,
//     this.buy,
//     this.ads,
//     this.rent,
//   });

//   String? link;
//   List<Free>? free;
//   List<Free>? flatrate;
//   List<Free>? buy;
//   List<Free>? ads;
//   List<Free>? rent;

//   factory Ca.fromRawJson(String str) => Ca.fromJson(json.decode(str));

//   String toRawJson() => json.encode(toJson());

//   factory Ca.fromJson(Map<String, dynamic> json) => Ca(
//         link: json["link"],
//         free: json["free"] == null
//             ? []
//             : List<Free>.from(json["free"]!.map((x) => Free.fromJson(x))),
//         flatrate: json["flatrate"] == null
//             ? []
//             : List<Free>.from(json["flatrate"]!.map((x) => Free.fromJson(x))),
//         buy: json["buy"] == null
//             ? []
//             : List<Free>.from(json["buy"]!.map((x) => Free.fromJson(x))),
//         ads: json["ads"] == null
//             ? []
//             : List<Free>.from(json["ads"]!.map((x) => Free.fromJson(x))),
//         rent: json["rent"] == null
//             ? []
//             : List<Free>.from(json["rent"]!.map((x) => Free.fromJson(x))),
//       );

//   Map<String, dynamic> toJson() => {
//         "link": link,
//         "free": free == null
//             ? []
//             : List<dynamic>.from(free!.map((x) => x.toJson())),
//         "flatrate": flatrate == null
//             ? []
//             : List<dynamic>.from(flatrate!.map((x) => x.toJson())),
//         "buy":
//             buy == null ? [] : List<dynamic>.from(buy!.map((x) => x.toJson())),
//         "ads":
//             ads == null ? [] : List<dynamic>.from(ads!.map((x) => x.toJson())),
//         "rent": rent == null
//             ? []
//             : List<dynamic>.from(rent!.map((x) => x.toJson())),
//       };
// }

// class Mx {
//   Mx({
//     this.link,
//     this.ads,
//   });

//   String? link;
//   List<Free>? ads;

//   factory Mx.fromRawJson(String str) => Mx.fromJson(json.decode(str));

//   String toRawJson() => json.encode(toJson());

//   factory Mx.fromJson(Map<String, dynamic> json) => Mx(
//         link: json["link"],
//         ads: json["ads"] == null
//             ? []
//             : List<Free>.from(json["ads"]!.map((x) => Free.fromJson(x))),
//       );

//   Map<String, dynamic> toJson() => {
//         "link": link,
//         "ads":
//             ads == null ? [] : List<dynamic>.from(ads!.map((x) => x.toJson())),
//       };
// }

// class In {
//   In({
//     this.link,
//     this.free,
//     this.ads,
//   });

//   String? link;
//   List<Free>? free;
//   List<Free>? ads;

//   factory In.fromRawJson(String str) => In.fromJson(json.decode(str));

//   String toRawJson() => json.encode(toJson());

//   factory In.fromJson(Map<String, dynamic> json) => In(
//         link: json["link"],
//         free: json["free"] == null
//             ? []
//             : List<Free>.from(json["free"]!.map((x) => Free.fromJson(x))),
//         ads: json["ads"] == null
//             ? []
//             : List<Free>.from(json["ads"]!.map((x) => Free.fromJson(x))),
//       );

//   Map<String, dynamic> toJson() => {
//         "link": link,
//         "free": free == null
//             ? []
//             : List<dynamic>.from(free!.map((x) => x.toJson())),
//         "ads":
//             ads == null ? [] : List<dynamic>.from(ads!.map((x) => x.toJson())),
//       };
// }
