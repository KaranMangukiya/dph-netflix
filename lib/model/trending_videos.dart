// To parse this JSON data, do
//
//     final trendingVideos = trendingVideosFromJson(jsonString);

import 'dart:convert';

class TrendingVideos {
  TrendingVideos({
    required this.id,
    required this.attributes,
  });

  int id;
  Attributes attributes;

  factory TrendingVideos.fromRawJson(String str) =>
      TrendingVideos.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory TrendingVideos.fromJson(Map<String, dynamic> json) => TrendingVideos(
        id: json["id"],
        attributes: Attributes.fromJson(json["attributes"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "attributes": attributes.toJson(),
      };
}

class Attributes {
  Attributes({
    required this.title,
    required this.summary,
    required this.url,
    required this.length,
    required this.censorRating,
    required this.showType,
    required this.trending,
    required this.isTrending,
    required this.createdAt,
    required this.updatedAt,
    required this.thumbnail,
    required this.releaseDate,
  });

  String title;
  String summary;
  String url;
  String length;
  String censorRating;
  String showType;
  String trending;
  bool isTrending;
  DateTime createdAt;
  DateTime updatedAt;
  String thumbnail;
  DateTime releaseDate;

  factory Attributes.fromRawJson(String str) =>
      Attributes.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Attributes.fromJson(Map<String, dynamic> json) => Attributes(
        title: json["title"],
        summary: json["summary"],
        url: json["url"],
        length: json["length"],
        censorRating: json["censor_rating"],
        showType: json["show_type"],
        trending: json["trending"],
        isTrending: json["is_trending"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        thumbnail: json["thumbnail"],
        releaseDate: DateTime.parse(json["release_date"]),
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "summary": summary,
        "url": url,
        "length": length,
        "censor_rating": censorRating,
        "show_type": showType,
        "trending": trending,
        "is_trending": isTrending,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "thumbnail": thumbnail,
        "release_date":
            "${releaseDate.year.toString().padLeft(4, '0')}-${releaseDate.month.toString().padLeft(2, '0')}-${releaseDate.day.toString().padLeft(2, '0')}",
      };
}
