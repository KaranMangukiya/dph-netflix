import 'package:flutter/material.dart';
import 'package:flutter_netflix/model/trending_videos.dart';
import 'package:go_router/go_router.dart';
import 'package:lucide_icons/lucide_icons.dart';
import 'package:shimmer/shimmer.dart';
import '../utils/utils.dart';
import 'bottom_sheet_button.dart';

class NetflixBottomSheet extends StatefulWidget {
  const NetflixBottomSheet({
    super.key,
    this.thumbnail,
    // required this.movie,
    this.trendingVideoData,
  });

  final String? thumbnail;
  // final Movie movie;
  final TrendingVideos? trendingVideoData;

  @override
  State<NetflixBottomSheet> createState() => _NetflixBottomSheetState();
}

class _NetflixBottomSheetState extends State<NetflixBottomSheet> {
  // TmdbVideos? videosData;

  final _shimmer = Shimmer(
    gradient: LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: <Color>[
          Colors.grey[900]!,
          Colors.grey[900]!,
          Colors.grey[800]!,
          Colors.grey[900]!,
          Colors.grey[900]!
        ],
        stops: const <double>[
          0.0,
          0.35,
          0.5,
          0.65,
          1.0
        ]),
    child: Row(
      children: const [
        Text(
          '2022',
          style: TextStyle(color: Colors.grey, fontSize: 14.0),
        ),
        SizedBox(
          width: 8.0,
        ),
        Text(
          '18+',
          style: TextStyle(color: Colors.grey, fontSize: 14.0),
        ),
        SizedBox(
          width: 8.0,
        ),
        Text(
          '10 Episodes',
          style: TextStyle(color: Colors.grey, fontSize: 14.0),
        ),
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    // return FutureBuilder(
    //     future: context
    //         .watch<TMDBRepository>()
    //         .getDetails(widget.movie.id, widget.movie.type),
    //     builder: (context, AsyncSnapshot<Movie> snapshoot) {
    // final movieDetails = widget.movie;
    final trendingData = widget.trendingVideoData!.attributes;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(4.0),
                    child: Image.asset(
                      'assets/netflix_symbol.png',
                      fit: BoxFit.contain,
                      width: 80.0,
                      height: 50,
                    ),
                  ),
                  Positioned(
                      top: 0,
                      left: 0,
                      child: Image.asset(
                        'assets/netflix_symbol.png',
                        width: 24.0,
                      )),
                ],
              ),
              const SizedBox(
                width: 8.0,
              ),
              Expanded(
                child: Column(
                  textDirection: TextDirection.ltr,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Flexible(
                          child: Column(
                            textDirection: TextDirection.ltr,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                widget.trendingVideoData!.attributes.title,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0),
                              ),
                              const SizedBox(
                                height: 8.0,
                              ),
                              // ignore: unnecessary_null_comparison
                              trendingData == null
                                  ? _shimmer
                                  : Row(
                                      children: [
                                        Text(
                                          '${trendingData.releaseDate.year}',
                                          style: const TextStyle(
                                              color: Colors.grey,
                                              fontSize: 14.0),
                                        ),
                                        const SizedBox(
                                          width: 8.0,
                                        ),
                                        const Text(
                                          '18+',
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 14.0),
                                        ),
                                        const SizedBox(
                                          width: 8.0,
                                        ),
                                        // Text(
                                        //   movieDetails?.getRuntime() ??
                                        //       '10 Episodes',
                                        //   style: const TextStyle(
                                        //       color: Colors.grey,
                                        //       fontSize: 14.0),
                                        // ),
                                      ],
                                    ),
                            ],
                          ),
                        ),
                        InkWell(
                          borderRadius: BorderRadius.circular(100.0),
                          radius: 32.0,
                          child: Container(
                            decoration: BoxDecoration(
                                color: bottomSheetIconColor,
                                borderRadius: BorderRadius.circular(100.0)),
                            child: const Icon(
                              LucideIcons.x,
                              size: 28.0,
                            ),
                          ),
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 8.0,
                    ),
                    Text(
                      widget.trendingVideoData!.attributes.summary,
                      maxLines: 4,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              BottomSheetButton(
                icon: Icons.play_arrow,
                label: 'Play',
                size: 32.0,
                padding:
                    const EdgeInsets.symmetric(horizontal: 4.0, vertical: 4.0),
                light: true,
                onTap: () {
                  context.go('/video', extra: trendingData);
                },
              ),
              const BottomSheetButton(
                icon: LucideIcons.download,
                label: 'Download',
              ),
              const BottomSheetButton(
                icon: LucideIcons.plus,
                label: 'My List',
              ),
              const BottomSheetButton(
                icon: LucideIcons.share2,
                label: 'Share',
              )
            ],
          ),
          const Divider(),
          InkWell(
            onTap: () {
              Navigator.pop(context);
              context.go('${GoRouter.of(context).location}/details',
                  extra: widget.trendingVideoData!
                  // extra: trendingData
                  );
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    const Icon(LucideIcons.info),
                    const SizedBox(
                      width: 8.0,
                    ),
                    Text(widget.trendingVideoData!.attributes.showType == 'tv'
                        ? 'Episodes & Info'
                        : 'Details & More'),
                  ],
                ),
                const Icon(LucideIcons.chevronRight)
              ],
            ),
          )
        ],
      ),
    );
    //       });
  }
}
