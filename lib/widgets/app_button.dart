import 'package:flutter/material.dart';
import 'package:flutter_netflix/constants/color.dart';

class AppButton extends StatelessWidget {
  const AppButton({
    super.key,
    this.title,
    this.textColor,
    this.backgroundColor,
    this.borderColor,
    this.buttonSize,
    this.fontSize,
    this.onPressed,
  });

  final String? title;
  final double? fontSize;
  final Color? textColor;
  final Color? borderColor;
  final Color? backgroundColor;
  final Size? buttonSize;
  final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        side: BorderSide(color: borderColor ?? AppColors.white),
        backgroundColor: backgroundColor,
        minimumSize: buttonSize,
      ),
      child: Text(
        title ?? '',
        style: TextStyle(
          color: AppColors.white,
          fontSize: 16,
        ),
      ),
    );
  }
}
