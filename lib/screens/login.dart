import 'package:flutter/material.dart';
import 'package:flutter_netflix/constants/color.dart';
import 'package:flutter_netflix/constants/variable.dart';
import 'package:flutter_netflix/repository/repository.dart';
import 'package:flutter_netflix/widgets/app_button.dart';
import 'package:flutter_netflix/widgets/text_field.dart';
import 'package:go_router/go_router.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  TMDBRepository repository = TMDBRepository();
  bool isUserName = false;
  bool isEmailError = false;
  bool isPasswordError = false;
  bool isRegister = false;

  bool emailvalidation(String email) {
    final bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
    return emailValid;
  }

  @override
  void dispose() {
    super.dispose();
    userNameController.dispose();
    emailController.dispose();
    passwordController.dispose();
  }

  Future<void> signUpUser() async {
    final FormState? form = formKey.currentState;
    FocusManager.instance.primaryFocus?.unfocus();
    if (form!.validate()) {
      var data = await repository.registerUser({
        'username': userNameController.text,
        'email': emailController.text,
        'password': passwordController.text,
      });
      if (data.isNotEmpty) {
        setState(() {
          isRegister = false;
        });
      }
    }
  }

  Future<void> signinUser() async {
    final FormState? form = formKey.currentState;
    FocusManager.instance.primaryFocus?.unfocus();
    if (form!.validate()) {
      var data = await repository.loginUser({
        'identifier': emailController.text,
        'password': passwordController.text,
      });
      if (data.isNotEmpty) {
        // ignore: use_build_context_synchronously
        context.go(
          '/profile',
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        body: Form(
          key: formKey,
          child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 100),
            child: ListView(
              children: [
                Image.asset('assets/netflix_logo.png'),
                const SizedBox(height: 50),
                if (isRegister)
                  AppTextField(
                    controller: userNameController,
                    labelText: 'Enter your username',
                    labelTextColor:
                        isEmailError ? AppColors.red : AppColors.white,
                    validator: (value) {
                      if (value!.isEmpty) {
                        setState(() {
                          isEmailError = true;
                        });
                        return 'Please enter a username';
                      } else {
                        setState(() {
                          isEmailError = false;
                        });
                      }
                      return null;
                    },
                  ),
                const SizedBox(height: 20),
                AppTextField(
                  controller: emailController,
                  labelText: 'Enter you email',
                  labelTextColor:
                      isEmailError ? AppColors.red : AppColors.white,
                  validator: (value) {
                    if (emailvalidation(value.toString()) != true ||
                        value!.isEmpty) {
                      setState(() {
                        isEmailError = true;
                      });
                      return 'Please enter a valid email';
                    } else {
                      setState(() {
                        isEmailError = false;
                      });
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 20),
                AppTextField(
                  controller: passwordController,
                  labelText: 'Password',
                  obscureText: true,
                  labelTextColor:
                      isPasswordError ? AppColors.red : AppColors.white,
                  validator: (value) {
                    if (value!.isEmpty) {
                      setState(() {
                        isPasswordError = true;
                      });
                      return 'Please enter a password';
                    } else {
                      setState(() {
                        isPasswordError = false;
                      });
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 20),
                AppButton(
                  backgroundColor: AppColors.transparent,
                  borderColor: AppColors.white,
                  buttonSize: const Size(double.infinity, 50),
                  fontSize: 16,
                  title: isRegister ? 'Sign Up' : 'Sign In',
                  textColor: AppColors.white,
                  onPressed: isRegister
                      ? () async {
                          await signUpUser();
                        }
                      : () async {
                          await signinUser();
                        },
                ),
                const SizedBox(height: 20),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      isRegister = !isRegister;
                      isEmailError = false;
                      isUserName = false;
                      isPasswordError = false;
                    });
                  },
                  child: Center(
                    child: Text(
                      isRegister ? 'Sign In' : "New to Netflix? Sign up now",
                      style: TextStyle(color: AppColors.white, fontSize: 16),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
