// ignore_for_file: use_build_context_synchronously, body_might_complete_normally_nullable
import 'package:flutter/material.dart';
import 'package:flutter_netflix/constants/color.dart';
import 'package:flutter_netflix/constants/variable.dart';
import 'package:flutter_netflix/model/user.dart';
import 'package:flutter_netflix/repository/repository.dart';
import 'package:flutter_netflix/utils/utils.dart';
import 'package:flutter_netflix/widgets/text_field.dart';
import 'package:go_router/go_router.dart';
import 'package:lucide_icons/lucide_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../widgets/profile_icon.dart';

class ProfileSelectionScreen extends StatefulWidget {
  const ProfileSelectionScreen({super.key});

  @override
  State<ProfileSelectionScreen> createState() => _ProfileSelectionScreenState();
}

class _ProfileSelectionScreenState extends State<ProfileSelectionScreen> {
  TMDBRepository repository = TMDBRepository();
  User? user;
  List<UserProfile> userProfile = [];
  bool isEdit = false;

  Future<void> getProfiles() async {
    final prefs = await SharedPreferences.getInstance();
    final id = prefs.getInt("user_id");
    final userData = await repository.loggedUserData(id ?? 1);

    setState(() {
      user = userData;
      userProfile = user!.profiles;
    });
  }

  @override
  void initState() {
    super.initState();
    getProfiles();
  }

  Future<void> showDialogBox(BuildContext context, User userData, int profileId,
      bool isEditProfile, bool isDelete) async {
    final size = MediaQuery.of(context).size;
    TMDBRepository repository = TMDBRepository();
    GlobalKey<FormState> formKey = GlobalKey<FormState>();

    Future<void> createProfile() async {
      if (formKey.currentState!.validate()) {
        var data = await repository.createProfile({
          "data": {
            "name": profileNameController.text,
            "users_permissions_user": userData.id,
          }
        });
        if (data.isNotEmpty) {
          await getProfiles();
          Navigator.pop(context);
          profileNameController.clear();
        }
      }
    }

    Future<void> editProfile() async {
      if (formKey.currentState!.validate()) {
        var data = await repository.editUserProfile(profileId, {
          "data": {
            "name": editProfileNameController.text,
          }
        });
        if (data.isNotEmpty) {
          await getProfiles();
          Navigator.pop(context);
          editProfileNameController.clear();
          setState(() {
            isEdit = false;
          });
        }
      }
    }

    Future<void> deleteProfile() async {
      var data = await repository.deleteUserProfile(profileId);
      if (data.isNotEmpty) {
        await getProfiles();
        Navigator.pop(context);
        setState(() {
          isEdit = false;
        });
      }
    }

    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: isDelete
            ? const Text('Are you sure to delete this profile?')
            : Text(isEditProfile ? 'Edit Profile' : 'Create Profile'),
        content: isDelete
            ? const SizedBox.shrink()
            : SizedBox(
                height: size.height / 8,
                child: Form(
                  key: formKey,
                  child: Column(
                    children: [
                      AppTextField(
                        hintText: 'Enter Your Name',
                        controller: isEditProfile
                            ? editProfileNameController
                            : profileNameController,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please enter a name';
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
        actions: [
          TextButton(
            onPressed: () async {
              isDelete
                  ? await deleteProfile()
                  : isEditProfile
                      ? await editProfile()
                      : await createProfile();
            },
            child: Text(
              isDelete
                  ? "Delete"
                  : isEditProfile
                      ? 'Edit'
                      : 'Create',
              style: TextStyle(color: AppColors.white),
            ),
          ),
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text(
              "Cancel",
              style: TextStyle(color: AppColors.white),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Image.asset(
          'assets/netflix_logo.png',
          height: 52.0,
          fit: BoxFit.fitHeight,
        ),
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () {
                setState(() {
                  isEdit = !isEdit;
                });
              },
              icon: const Icon(LucideIcons.pencil))
        ],
        elevation: 0.0,
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'Who\'s Watching?',
            style: TextStyle(fontSize: 18.0),
          ),
          GridView.builder(
              padding:
                  const EdgeInsets.symmetric(horizontal: 64.0, vertical: 32.0),
              shrinkWrap: true,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 1.0,
                mainAxisSpacing: 32.0,
                crossAxisSpacing: 8.0,
                crossAxisCount: 2,
              ),
              itemCount: userProfile.length,
              itemBuilder: (BuildContext ctx, index) {
                return GestureDetector(
                  onTap: () {
                    // context
                    //     .read<ProfileSelectorBloc>()
                    //     .add(SelectProfile(index));
                    context.go('/home');
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Align(
                          alignment: Alignment.center,
                          child: AspectRatio(
                            aspectRatio: 1,
                            child: Stack(
                              alignment: Alignment.topRight,
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(top: 10),
                                  child: ProfileIcon(
                                    color: profileColors[index],
                                  ),
                                ),
                                if (isEdit)
                                  GestureDetector(
                                    onTap: () {
                                      showDialogBox(context, user!,
                                          userProfile[index].id, false, true);
                                    },
                                    child: Icon(
                                      Icons.delete,
                                      color: AppColors.red,
                                    ),
                                  ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 8.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(userProfile[index].name),
                          const SizedBox(width: 5),
                          if (isEdit)
                            GestureDetector(
                              onTap: () {
                                showDialogBox(context, user!,
                                    userProfile[index].id, true, false);
                              },
                              child: const Icon(
                                Icons.edit,
                                size: 15,
                              ),
                            )
                        ],
                      ),
                    ],
                  ),
                );
              }),
          if (userProfile.length < 5)
            GestureDetector(
              onTap: () {
                showDialogBox(context, user!, 1, false, false);
              },
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 2),
                        color: AppColors.transparent),
                    child: const Icon(Icons.add),
                  ),
                  const Text('Create Profile'),
                ],
              ),
            ),
        ],
      ),
    );
  }
}

class Smile extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var center = size / 2;
    var paint = Paint()..color = Colors.white;

    canvas.drawCircle(Offset(size.width * .20, center.height * .50),
        size.shortestSide * .05, paint);

    canvas.drawCircle(Offset(size.width * .80, center.height * .50),
        size.shortestSide * .05, paint);

    var startPoint = Offset(size.width * .25, size.height / 2);
    var controlPoint1 = Offset(size.width * .3, size.height / 1.6);
    var controlPoint2 = Offset(size.width * .6, size.height / 1.6);
    var endPoint = Offset(size.width * .8, size.height / 2);

    var path = Path();
    path.moveTo(startPoint.dx, startPoint.dy);

    path.cubicTo(controlPoint1.dx, controlPoint1.dy, controlPoint2.dx,
        controlPoint2.dy, endPoint.dx, endPoint.dy);

    path.cubicTo(size.width * .8, size.height / 2, size.width * .86,
        size.height / 2, size.width * .8, size.height / 1.8);

    path.cubicTo(size.width * .6, size.height / 1.4, size.width * .3,
        size.height / 1.46, size.width * .25, size.height / 1.7);

    path.cubicTo(size.width * .25, size.height / 1.68, size.width * .18,
        size.height / 2, size.width * .25, size.height / 2);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(Smile oldDelegate) => false;
  @override
  bool shouldRebuildSemantics(Smile oldDelegate) => false;
}
