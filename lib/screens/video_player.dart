import 'package:flutter/material.dart';
import 'package:flutter_netflix/constants/color.dart';
import 'package:flutter_netflix/model/trending_videos.dart';
import 'package:go_router/go_router.dart';
import 'package:video_player/video_player.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:chewie/chewie.dart';

class VideoApp extends StatefulWidget {
  const VideoApp({Key? key, this.videoData}) : super(key: key);
  final Attributes? videoData;

  @override
  // ignore: library_private_types_in_public_api
  _VideoAppState createState() => _VideoAppState();
}

class _VideoAppState extends State<VideoApp> {
  YoutubePlayerController? youtubeController;
  VideoPlayerController? _controller;
  ChewieController? _chewieController;
  bool isYoutube = false;

  bool isYoutubeUrl(String url) {
    final youtubeRegex =
        RegExp(r'^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+');
    return youtubeRegex.hasMatch(url);
  }

  @override
  void initState() {
    super.initState();
    if (isYoutubeUrl(widget.videoData!.url)) {
      setState(() {
        isYoutube = true;
      });
      final videoId = YoutubePlayer.convertUrlToId(widget.videoData!.url);
      youtubeController = YoutubePlayerController(
        initialVideoId: videoId.toString(),
        flags: const YoutubePlayerFlags(
          autoPlay: false,
          controlsVisibleAtStart: true,
        ),
      );
    } else {
      setState(() {
        isYoutube = false;
      });

      _controller = VideoPlayerController.network(
        widget.videoData!.url,
      )..setLooping(false);

      _controller!.initialize().then((_) {
        setState(() {});
      });
      _chewieController = ChewieController(
        videoPlayerController: _controller!,
        aspectRatio: 16 / 9,
        autoPlay: false,
        looping: false,
        autoInitialize: true,
        materialProgressColors:
            ChewieProgressColors(bufferedColor: AppColors.red),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    youtubeHierarchy() {
      return Align(
        alignment: Alignment.center,
        child: FittedBox(
          fit: BoxFit.fill,
          child: YoutubePlayer(
            controller: youtubeController!,
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            GoRouter.of(context).go('/home');
          },
          child: const Icon(Icons.arrow_back),
        ),
      ),
      body: !isYoutube
          ? _controller!.value.isInitialized
              ? Center(
                  child: Chewie(
                    controller: _chewieController!,
                  ),
                )
              : Center(
                  child: CircularProgressIndicator(
                    color: AppColors.white,
                  ),
                )
          : OrientationBuilder(
              builder: (BuildContext context, Orientation orientation) {
              if (orientation == Orientation.landscape) {
                return youtubeHierarchy();
              } else {
                return youtubeHierarchy();
              }
            }),
    );
  }

  @override
  void dispose() {
    super.dispose();
    youtubeController?.dispose();
    _controller?.dispose();
    _chewieController?.dispose();
  }
}
